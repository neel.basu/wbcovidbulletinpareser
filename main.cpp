#include <iostream>
#include <regex>
#include <xlnt/xlnt.hpp>

// use https://document.online-convert.com/convert-to-xlsx to convert pdf to xslx

std::vector<std::string> search_streets(const std::string& subject){
    auto flags = std::regex::ECMAScript | std::regex::extended | std::regex::icase;
    std::regex street_pattern("(?:\\s*[\\w\\.\\'\\&]+)+\\s+(?:street|st|road|rd|sarani|avenue|bylane|byelane|lane|ln|bustee|place|point|polly|para|colony|more|clinic|hospital|hostel|home|bajar|bazar|pota|pathway|pur|nagar|talab|ghat|ghata|ship|township|town|block|park|society|abasan|housing|complex|bagan)?", flags);
    std::smatch match;
    std::string::const_iterator begin = subject.cbegin();
    std::vector<std::string> results;
    while(std::regex_search(begin, subject.cend(), match, street_pattern)){
        std::string matched = match.str();
        std::string result = std::regex_replace(matched, std::regex("\\s+", flags), " "); 
        result.erase(result.find_last_not_of(",\t\n\v\f\r ")+1);
        result.erase(0, result.find_first_not_of(",\t\n\v\f\r "));
        results.push_back(result);
        begin = match.suffix().first;
        
        std::cout << "UNMATCHED: " << match.prefix().str() << std::endl;
        std::cout << "MATCHED Level 2: "   << result << std::endl;
    }
    return results;
}

std::vector<std::string> search_addresses(const std::string& subject){
    auto flags = std::regex::ECMAScript | std::regex::extended | std::regex::icase;
    std::regex house_pattern("[\\.\\,\\s]\\d{1,3}(?:[ \\w]{0,3}\\W\\s*)+[\\s\\w\\'\\&]+(?:street|st|road|rd|sarani|avenue|bylane|byelane|lane|ln|bustee|place|point|polly|para|colony|more|clinic|hospital|hostel|home|bajar|bazar|pota|pathway|pur|nagar|talab|ghat|ghata|ship|township|town|block|park|society|abasan|housing|complex|bagan)?", flags); // The subject has to be preceeded by a space
    std::smatch match;
    std::string::const_iterator begin = subject.cbegin();
    std::vector<std::string> results;
    while(std::regex_search(begin, subject.cend(), match, house_pattern)){
        std::string matched = match.str();
        std::string result = std::regex_replace(matched, std::regex("\\s+", flags), " "); 
        result.erase(result.find_last_not_of(",\t\n\v\f\r ,")+1);
        result.erase(0, result.find_first_not_of(",\t\n\v\f\r "));
        results.push_back(result);
        begin = match.suffix().first;
        std::cout << "NOT MATCHED" << match.prefix().str() << std::endl;
        std::vector<std::string> streets_found = search_streets(match.prefix().str());
        results.insert(results.end(), streets_found.cbegin(), streets_found.cend());
        std::cout << "MATCHED Level 1: " << result << std::endl;
    }
    std::string rest(begin, subject.cend());
    std::vector<std::string> streets_found = search_streets(rest);
    results.insert(results.end(), streets_found.cbegin(), streets_found.cend());
    return results;
}

std::string clean_subject(const std::string& subject){
    auto flags = std::regex::ECMAScript | std::regex::extended | std::regex::icase;
    std::string cleaned = " "+subject;
    std::replace(cleaned.begin(), cleaned.end(), '(', ',');
    std::replace(cleaned.begin(), cleaned.end(), ')', ',');
    std::regex pincode_pattern("(?:kol|pin)(?:kata|code)?\\s*\\-?\\s*(\\d{0,6})", flags);
    cleaned = std::regex_replace(cleaned, pincode_pattern, "kolkata-$1");
    return std::regex_replace(cleaned, std::regex("\\s+"), " ");
}

int main(int argc, char** argv) {
    if(argc < 2){
        std::cout << "Please specify the input xlsx file" << std::endl;
        std::cout << "\t" << argv[0] << " /path/to/bulletin.xlsx" << std::endl;
        return 1;
    }
    
    xlnt::row_properties original_style;
    original_style.custom_height = true;
    original_style.height = 80.0;
    original_style.style  = 26;
    xlnt::alignment original_address_alignment;
    original_address_alignment.wrap(true);
    
    xlnt::workbook inbook, outbook;
    inbook.load(argv[1]);
    auto sheet_in  = inbook.active_sheet();
    auto sheet_out = outbook.active_sheet();
    
    sheet_out.title("extracted");
    outbook.create_builtin_style(26);
    
    std::size_t counter = 0, empty_rows = 0;
    for(auto row : sheet_in.rows(false)){
        auto serial    = row[0];
        auto borough   = row[1];
        auto area      = row[2];
        auto wards     = row[3];

        std::string area_name = clean_subject(area.value<std::string>());
        if(counter > 0){
            sheet_out.cell(xlnt::cell_reference(1, counter+1)).value("AREA_BULLETIN");
            sheet_out.cell(xlnt::cell_reference(2, counter+1)).value(serial.value<int>());
            sheet_out.cell(xlnt::cell_reference(3, counter+1)).value(borough.value<int>());
            sheet_out.cell(xlnt::cell_reference(4, counter+1)).value(wards.value<std::string>());
            sheet_out.cell(xlnt::cell_reference(5, counter+1)).value(area_name);
                        
            std::cout << serial.value<int>() << ": " << area_name << std::endl;
            sheet_out.cell(xlnt::cell_reference(5, counter+1)).alignment(original_address_alignment);
            sheet_out.cell(xlnt::cell_reference(5, counter+1)).fill(xlnt::fill::solid(xlnt::color::green()));
            
            sheet_out.add_row_properties(counter+1, original_style);
        }else{
            sheet_out.cell(xlnt::cell_reference(1, counter+1)).value("");
            sheet_out.cell(xlnt::cell_reference(2, counter+1)).value("serial");
            sheet_out.cell(xlnt::cell_reference(3, counter+1)).value("borough");
            sheet_out.cell(xlnt::cell_reference(4, counter+1)).value("wards");
            sheet_out.cell(xlnt::cell_reference(5, counter+1)).value("area");
        }
        ++counter;
        
        std::vector<std::string> results = search_addresses(area_name);
        for(const std::string& result: results){
            sheet_out.cell(xlnt::cell_reference(1, counter+1)).value("AREA_EXTRACTED");
            sheet_out.cell(xlnt::cell_reference(2, counter+1)).value(serial.value<int>());
            sheet_out.cell(xlnt::cell_reference(3, counter+1)).value(borough.value<int>());
            sheet_out.cell(xlnt::cell_reference(4, counter+1)).value(wards.to_string());
            sheet_out.cell(xlnt::cell_reference(5, counter+1)).value(result);
            
            ++counter;
        }
               
        if(serial.to_string().empty() && area_name.empty()){
            ++empty_rows;
            continue;
            if(empty_rows > 3){
                break;
            }
        }
    }
    
    outbook.save("output.xlsx");

    return 0;
}
