cmake_minimum_required(VERSION 3.0)

project(wbcovidbulletinpareser)

add_subdirectory(deps)

add_executable(wbcovidbulletinpareser main.cpp)
target_link_libraries(wbcovidbulletinpareser xlnt)

install(TARGETS wbcovidbulletinpareser RUNTIME DESTINATION bin)
