Conver kolkata pdf files using https://document.online-convert.com/convert-to-xlsx
Download that xlsx file
Remove all occurences of headers inside the xlsx generated
Then use that file as input

# Build

```
git clone https://gitlab.com/neel.basu/wbcovidbulletinpareser.git
cd wbcovidbulletinpareser
git submodule update --init --recursive
mkdir build
cd build
cmake ..
make
```

# Run

```
./wbcovidbulletinpareser /path/to/buliten.xlsx
```

output.xlsx will be generated